from bs4 import BeautifulSoup
import requests
import re
from selenium import webdriver
import xml.etree.ElementTree as ET

universalPassword = ""

def parseConfig(p):
	tree = ET.parse("config.xml")
	p = tree.getroot().find("UniversalPassword").text	

class RedditAccountCreator:
	def __init__(self):
		self.__account = {"Username" : "", "Email" : "", "Domain" : ""}

	def getUsername(self):
		return self.__account["Username"]
	def setUsername(self, username):
		self.__account["Username"] = username

	def setEmail(self, email):
		self.__account["Email"] = email
	def getEmail(self):
		return self.__account["Email"]
	
	def setDomain(self, domain):
		self.__account["Domain"] = domain
	def getDomain(self):
		return self.__account["Domain"]

	def addAccount(self, username, email):
		tree = ET.parse("config.xml")
		accounts = tree.getroot().find("Accounts")
		account = ET.SubElement(accounts, "Account")

		username = ET.SubElement(account, "Username")
		username.text = self.getUsername()

		email = ET.SubElement(account, "Email")
		email.text = self.getEmail()
		tree.write("config.xml")

	def userNameGenerator(self):
		r = requests.get("http://www.fakenamegenerator.com")
		email = None
		username = None
		domain = None

		if r != None:
			bs = BeautifulSoup(r.text)
			try:
				email = bs.find("li", {"class" : "email"}).find("span", {"class":"value"}).string
			except Exception as e:
				pass
			finally:
				return False
		username = email.split("@")[0]
		domain = email.split("@")[1]

		r = requests.get("http://www.reddit.com/api/username_available.json?user=" + username)
		if r.status_code != 200:
			return False
		if r.text != "true":
			self.userNameGenerator()

		self.setUsername(username)
		self.setEmail(email)
		self.setDomain(domain)
		return True
	
	def getCaptcha(self):
		r = requests.post("http://www.reddit.com/api/new_captcha", {"renderstyle" : "html"})
		if r.status_code != 200:
			return False
		pngString = r.json['jquery'][11][3][0]
		return "http://www.reddit.com/api/captcha/" + pngString + ".png"

	def registerAccount(self):
		return

	def verifyAccount(self):
		r = requests.get("http://www.fakemailgenerator.com/?e=" + self.getEmail() + "/")
		if r.status_code != 200:
			return False
		while(1):
			r = requests.get("http://www.fakemailgenerator.com/inbox/" + self.getDomain() + "/" + self.getUsername().lower())
			if r.status_code != 200:
				continue
			soup = BeautifulSoup(r.text)
			emailLink = soup.find("iframe" , {"id" : "emailFrame"})
			if emailLink is None:
				continue
			emailLink = emailLink.get("src")
			r = requests.get(emailLink)
			if r.status_code != 200:
				continue
			verifyLink = re.search("http://reddit.com/verification/.*y", r.text)
			if verifyLink is None:
				continue
			verifyLink = verifyLink.group()
			browser = self.login()
			if browser == None:
				return False
			browser.get(verifyLink)
			break
			
	def sendVerification(self):
		browser = self.login()
		if browser is None:
			return False
		browser.get("http://www.reddit.com/verify")
		curpass = browser.find_element_by_name("curpass")
		if curpass is None:
			return False
		curpass.send_keys(universalPassword)
		curpass.submit()
		return True

	def login(self):
		browser = webdriver.Firefox()
		browser.get("http://www.reddit.com/login")
		try:
			userbox = browser.find_element_by_id("user_login")
			userbox.send_keys(self.getUsername())
			passbox = browser.find_element_by_id("passwd_login")
			passbox.send_keys(universalPassword)
			passbox.submit()
			return browser
		except Exception:
			pass
		finally:
			return None

if __name__ == '__main__':
	parseConfig(universalPassword)
